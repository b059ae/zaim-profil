<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Class ApplicationForm
 *
 * @package frontends\asdfinans\models
 * @author Vladimir Yants <vyants@dengisrazy.ru>
 * @copyright 2015, ГК ДеньгиСразу <dengisrazy.ru>
 * @since 3.0.8
 */
class ApplicationForm extends Model
{
    const INDIVIDUAL = 0;
    const LEGAL_ENTITY = 1;
    const SOLE_PROPRIETOR = 2;

    const PAYMENTS_PERCENT = 0;
    const PAYMENTS_ANNUITY = 1;

    public $name;
    public $subject_type;
    public $payments_type = 0;
    public $city;
    public $phone;
    public $sum;
    public $description;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'subject_type', 'city', 'phone', 'sum', 'description'], 'required'],

            [['name'], 'string', 'max' => 100],
            [['city'], 'string', 'max' => 30],
            [['description'], 'string', 'max' => 400],

            ['sum', 'number'],

            ['phone', 'filter', 'filter' => function ($value) {
                return str_replace([' ', ')', '(', '-'], '', $value);
            }],
            ['phone', 'match',
                'pattern' => '/^\+7\d{10}$/',
                'message' => 'Некорректный формат телефона'
            ],

            ['subject_type', 'in',
                'range' => array_keys(self::getSubjectTypesList()),
                'message' => 'Неизвестный тип субъекта'
            ],
            ['payments_type', 'in',
                'range' => array_keys(self::getPaymentsTypesList()),
                'message' => 'Неизвестный тип платежей'
            ],
            [
                'sum',
                'compare',
                'compareValue' => Option::get('draggers_sum_from'),
                'operator' => '>=',
                'message' => 'Минимальная сумма займа - '.Option::get('draggers_sum_from').' руб.'
            ],
            [
                'sum',
                'compare',
                'compareValue' => Option::get('draggers_sum_to'),
                'operator' => '<=',
                'message' => 'Максимальная сумма займа - '.Option::get('draggers_sum_to').' руб.'
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'subject_type' => 'Тип субъекта',
            'city' => 'Город',
            'phone' => 'Моб. телефон',
            'sum' => 'Сумма займа',
            'description' => 'Описание залога',
        ];
    }

    /**
     * @return array
     * @static
     */
    public static function getSubjectTypesList()
    {
        return [
            self::INDIVIDUAL => 'Физ. лицо',
            self::LEGAL_ENTITY => 'Юр. лицо',
            self::SOLE_PROPRIETOR => 'ИП',
        ];
    }

    /**
     * @return array
     * @static
     */
    public static function getPaymentsTypesList()
    {
        return [
            self::PAYMENTS_PERCENT => 'Ежемесячное погашение только процентов, а сумма основного долга выплачивается в конце срока.',
            self::PAYMENTS_ANNUITY => 'Ежемесячное погашение равными частями основного долга, и процентов.',
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function get1CFormattedPhone()
    {
        $result = preg_match('/^\+7(\d{3})(\d{3})(\d{2})(\d{2})$/', $this->phone, $matches);
        if (!$result) {
            throw new \Exception('Невозможно форматирвоать телефон, неверный формат');
        }

        return "8({$matches[1]}){$matches[2]}-{$matches[3]}-{$matches[4]}";
    }

    public function save(){
        $subdomain = Yii::$app->subdomains->getCurrent();

        // save to DB
        $model = new Applications();
        $model->name = $this->name;
        $model->phone = preg_replace('[^0-9\+]', '', $this->phone);
        $model->subject_type = $this->subject_type;
        $model->payments_type = $this->payments_type;
        $model->city = $this->city;
        $model->sum = $this->sum;
        $model->description = $this->description;
        $model->domain = $subdomain->domain;
        $model->save();

        // send Email
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Оформление заявки')
            ->setTextBody(
                $this->getAttributeLabel('name').': '.$this->name."\n".
                $this->getAttributeLabel('phone').': '.$this->phone."\n".
                $this->getAttributeLabel('city').': '.$this->city."\n".
                $this->getAttributeLabel('sum').': '.$this->sum."\n".
                $this->getAttributeLabel('description').': '.$this->description."\n".
                'Домен: '.$subdomain->domain."\n"
            )
            ->send();

        return true;
    }
}
