<?php

namespace common\models;

use common\models\Option;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class CallOrderForm
 * @package backend\modules\company\modules\loan\models
 * @author Кривенко Валерий <vkrivenko@dengisrazy.ru>
 * @copyright 2015, ГК ДеньгиСразу <dengisrazy.ru>
 * @version 1.0.0
 * @since 3.0.22
 */
class CallOrderForm extends Model
{
    /**
     * Телефон заказчика услуги
     * @var null|string $phone
     */
    public $phone;

    /**
     * ФИО заказчика услуги
     * @var null|string $name
     */
    public $name;

    /**
     * Символьный код сайта
     * @var null $app
     */
    protected $app_name;

    /**
     * Идентификатор услуги для колцентра
     * @var null $service_id
     */
    protected $service_id;

    /**
     * Ссылка отправки заявки
     * @var null $service_url
     */
    protected $service_url;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->app_name = 'asdfinans';

        //Если на записаны настройки для данного сервиса
        if (is_null(Option::get('call_order_service_active'))) {
            throw new InvalidConfigException('You should specify call order params in editable-settings');
        }

        $this->service_id = Option::get('call_order_service_id');
        $this->service_url = Option::get('call_order_service_url');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'name'], 'string'],
            ['time', 'safe'],
            ['phone', function () {
                if (!preg_match('/^\+7\d{10}$/', $this->cleanPhone($this->phone))) {
                    $this->addError('phone', 'Номер телефона должен быть в международном формате');
                }
            }],
            /*[
                'time',
                function ($attribute) {
                    $formatter = Yii::$app->getFormatter();
                    $call_from = $formatter->asTime($this->getScheduleCallBegin(), 'php:H-i');
                    $call_to = $formatter->asTime($this->getScheduleCallEnd(), 'php:H-i');

                    if (!$this->isActive()) {
                        $this->addError(
                            $attribute,
                            "Сервис «Заказать звонок» на данный момент отключен.\nПриносим свои извинения"
                        );
                    }

                    if (!$this->isCallActive()) {
                        $this->addError(
                            $attribute,
                            "Сервис «Заказать звонок» работает с {$call_from} по {$call_to} по московскому времени."
                            . "\nПриносим свои извинения."
                        );
                    }
                }
            ],*/
            //Все валидаторы обязательности полей должны проверяться только на сервере, но не на клиенте
            [
                ['phone', 'name'],
                'required',
                'when' => function () { return true; },
                'whenClient' => 'function () { return false; }',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone' => 'Контактный телефон',
            'name' => 'Ваше имя'
        ];
    }

    /**
     * Виртуальный атрибут, по которому происходит основная проверка на открытость и возможность отправки в колцентр.
     * Чтобы можно были применять правила валидации через атрибут.
     * @return int
     */
    public function getTime()
    {
        return time();
    }

    /**
     * Метод пытается определить активность сервиса в целом
     * @return bool
     */
    public function isActive()
    {
        return (bool)Option::get('call_order_service_active');
    }

    /**
     * Метод пытается определить активность формы.
     * Это нужно для того, чтобы понять стоит ли ее выводить во фронт (зависит от рамок расписания)
     * @return bool
     */
    public function isFormActive()
    {
        $now = $this->getNowWithZone();
        $now = $this->omitDate($now)->getTimestamp();

        return ($this->isActive() && $now >= $this->getScheduleFormBegin() && $now < $this->getScheduleFormEnd());
    }

    /**
     * @return bool
     */
    public function isCallActive()
    {
        $now = $this->getNowWithZone();
        $now = $this->omitDate($now)->getTimestamp();

        return ($this->isActive() && $now >= $this->getScheduleCallBegin() && $now < $this->getScheduleCallEnd());
    }

    /**
     * Метод пытается вернуть timestamp начала показа формы
     * @return \DateTime|int
     */
    public function getScheduleFormBegin()
    {
        return $this->getScheduleOption('call_order_schedule_form_begin');
    }

    /**
     * Метод пытается вернуть timestamp окончания показа формы
     * @return \DateTime|int
     */
    public function getScheduleFormEnd()
    {
        return $this->getScheduleOption('call_order_schedule_form_end');
    }

    /**
     * Метод пытается вернуть timestamp начала обзовона
     * @return \DateTime|int
     */
    public function getScheduleCallBegin()
    {
        return $this->getScheduleOption('call_order_schedule_call_begin');
    }

    /**
     * Метод пытается вернуть timestamp окончания обзвона
     * @return \DateTime|int
     */
    public function getScheduleCallEnd()
    {
        return $this->getScheduleOption('call_order_schedule_call_end');
    }

    /**
     * Метод подготавливает URL для запроса в колцентр
     * @return string
     */
    public function createRequest()
    {
        $phone = urlencode($this->cleanPhone($this->phone));
        $name = urlencode($this->name);
        return "{$this->service_url}?campaign={$this->service_id}&phone={$phone}&name={$name}";
    }

    private function getNowWithZone()
    {
        $date = new \DateTime;
        $date->setTimezone(new \DateTimeZone('Europe/Moscow'));

        return $date;
    }

    /**
     * @param \DateTime $date
     * @return \DateTime
     */
    private function omitDate(\DateTime $date)
    {
        return $date->setDate(date('Y'), 1, 1);
    }

    /**
     * @param \DateTime $date
     * @return \DateTime
     */
    private function omitTime(\DateTime $date)
    {
        return $date->setTime(0,0,0);
    }

    /**
     * @param $option
     * @return \DateTime|int
     */
    private function getScheduleOption($option)
    {
        $option = Option::get($option);
        $option = (new \DateTime)->setTimestamp(strtotime($option));
        $option = $this->omitDate($option)->getTimestamp();

        return $option;
    }

    /**
     * @param string $value
     * @return string
     */
    private function cleanPhone($value)
    {
        return str_replace([' ', '(', ')', '-'], '', $value);
    }

    public function save(){
        $subdomain = Yii::$app->subdomains->getCurrent();

        // save to DB
        $model = new Applications();
        $model->name = $this->name;
        $model->phone = preg_replace('[^0-9\+]', '', $this->phone);
        $model->domain = $subdomain->domain;
        $model->save();

        // send Email
        Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['supportEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Заказ обратного звонка')
            ->setTextBody(
                $this->getAttributeLabel('name').': '.$this->name."\n".
                $this->getAttributeLabel('phone').': '.$this->phone."\n".
                'Домен: '.$subdomain->domain."\n"
            )
            ->send();

        return true;
    }
}
