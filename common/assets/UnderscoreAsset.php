<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class UnderscoreAsset
 * @package common\assets
 * @author German Sokolov
 */
class UnderscoreAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/underscore';

    public $css = [];

    public $js = [
        'underscore.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
