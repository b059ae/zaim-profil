<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@private-files-dir' => dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'private-files',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            //'viewPath' => '@common/message/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.dengisrazy.ru',
                'username' => 'info@zaim-profil.ru',
                'password' => '9rznRpRHKRNC',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
    ],
];
