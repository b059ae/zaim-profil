<?php
return [
    'adminEmail' => 'info@zaim-profil.ru',
    'supportEmail' => 'info@zaim-profil.ru',
    'user.passwordResetTokenExpire' => 3600,
];
