<?php

namespace common\components\helpers;

use yii\base\UnknownClassException;


class UtilsHelper
{
    /**
     * Метод проверяет значение на пустоту.
     * Основная идея заключается в том, что если проверять значение через empty(),
     * он расценивает валидный НОЛЬ как пустоту, но иногда ноль должен быть принят, в то время как "" нет.
     * @param string $value
     * @return bool
     */
    public static function isValueEmpty($value)
    {
        if (is_array($value)) {
            return (count($value) == 0);
        }
        if (is_object($value)) {
            return false;
        }

        return (empty($value) && strlen($value) == 0);
    }
}
