<?php

namespace common\components\sitemap;

use yii\base\Exception;

/**
 * Class SiteMapCrawler
 *
 * @author German Sokolov
 * @package common\components\sitemap
 */
class SiteMapCrawler extends \PHPCrawler
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $last_mod;

    /**
     * @var string
     */
    public $change_freq;

    /**
     * @var resource
     */
    public $file_handle;

    /**
     * @var array
     */
    public $disallow = [];

    /**
     * @var array
     */
    private $urls = [];

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->PageRequest = new SiteMapHTTPRequest();
        $this->PageRequest->setHeaderCheckCallbackFunction($this, 'handleHeaderInfo');
    }

    /**
     * @var array
     */
    public $content_type = [
        'text/html',
        'application/pdf',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-excel',
        'application/vnd.ms-office',
        'application/x-7z-compressed',
        'application/zip',
        'application/gzip',
        'application/tar',
        'application/x-gzip',
        'application/x-tar',
    ];

    /**
     * @param \PHPCrawlerDocumentInfo $page_info
     * @return void
     * @throws Exception
     */
    public function handleDocumentInfo(\PHPCrawlerDocumentInfo $page_info)
    {
        if (isset($this->urls[$page_info->url])) {
            return;
        }
        $this->urls[$page_info->url] = true;

        if (!$this->last_mod) {
            throw new Exception('Нужно ввести last_mod');
        }
        if ($page_info->error_occured) {
            throw new Exception(
                "Ошибка подключения Crawler к странице:\nАдрес:{$page_info->url}\nОшибка:{$page_info->error_string}\n\n"
            );
        }

        $tab = '    ';

        $http_status = (string)$page_info->http_status_code;
        $first_digit = $http_status[0] ?? null;
        $relative_url = $page_info->path . $page_info->file;
        $content_type = $page_info->content_type;
        if (($first_digit == '2' || $first_digit == '3')&& $this->isAllowedContentType($content_type) && $this->isAllowedUrl($relative_url)) {
            $url = rtrim($this->domain, '/') . $relative_url;
            $priority = ($relative_url == '/') ? '0.9' : '0.8';

            $url_tag = '';
            $url_tag .= "{$tab}<url>\n";
            $url_tag .= "{$tab}{$tab}<loc>{$url}</loc>\n";
            $url_tag .= "{$tab}{$tab}<lastmod>{$this->last_mod}</lastmod>\n";
            $url_tag .= "{$tab}{$tab}<changefreq>{$this->change_freq}</changefreq>\n";
            $url_tag .= "{$tab}{$tab}<priority>{$priority}</priority>\n";
            $url_tag .= "{$tab}</url>\n";

            fwrite($this->file_handle, $url_tag);
        }
    }

    /**
     * @param string $content_type
     * @return bool
     */
    private function isAllowedContentType(string $content_type): bool
    {
        return in_array($content_type, $this->content_type);
    }

    /**
     * @param string $url
     * @return bool
     */
    private function isAllowedUrl(string $url): bool
    {
        foreach ($this->disallow as $disallow) {
            $haystack = trim($disallow, '/');
            if (strpos($haystack, $url) !== false) {
                return false;
            }
        }

        return true;
    }
}
