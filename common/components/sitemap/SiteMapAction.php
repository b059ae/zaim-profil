<?php

namespace common\components\sitemap;

use common\components\subdomains\Subdomains;
use yii\base\Action;

/**
 * Class SiteMapAction
 * @author German Sokolov
 * @package common\components\sitemap\actions
 */
class SiteMapAction extends Action
{
    /**
     * @var Subdomains
     */
    public $subdomains;

    /**
     * @var string
     */
    public $sites_maps_path;

    /**
     * @var string
     */
    public $file_name;

    /**
     *
     */
    public function run()
    {
        $subdomain = $this->subdomains->getSubdomain(false);
        $path = $this->sites_maps_path . '/' . ($subdomain ? "$subdomain." : '') . $this->file_name;

        if (!file_exists(\Yii::getAlias('@private-files-dir') . $path)) {
            throw new \yii\web\NotFoundHttpException('Файл не найден на сервере.');
        }

        return \Yii::$app->response->sendFile(
            \Yii::getAlias('@private-files-dir') . $path,
            $this->file_name,
            [
                'xHeader' => 'X-Accel-Redirect',
                'inline' => true,
            ]
        );
    }
}
