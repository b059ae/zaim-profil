<?php

namespace common\components\sitemap;

use yii\base\Object;

/**
 * Class SiteMapGenerator
 * @author German Sokolov
 * @package common\components\sitemap
 */
class SiteMapGenerator extends Object
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $last_mod;

    /**
     * @var string
     */
    public $change_freq = 'monthly';

    /**
     * @var string
     */
    public $output_path;

    /**
     * @var array
     */
    public $disallow = [];

    /**
     *
     */
    public function generate()
    {
        $handle = fopen($this->output_path, 'w');
        fwrite($handle, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        fwrite($handle, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n");

        $crawler = new SiteMapCrawler;
        $crawler->file_handle = $handle;
        $crawler->disallow = $this->disallow;
        $crawler->domain = $this->domain;
        $crawler->last_mod = $this->last_mod;
        $crawler->change_freq = $this->change_freq;

        $crawler->setURL($this->domain);
        $crawler->go();

        fwrite($handle, "</urlset>\n\n");
        fclose($handle);
    }
}
