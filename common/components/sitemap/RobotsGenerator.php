<?php

namespace common\components\sitemap;

use yii\base\Object;

/**
 * Class RobotsGenerator
 * @author German Sokolov
 * @package common\components\sitemap
 */
class RobotsGenerator extends Object
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $output_path;

    /**
     * @var array
     */
    public $disallow;

    /**
     *
     */
    public function generate()
    {
        $disallows = array_merge($this->disallow, [
            '/*openstat*',
            '/*utm*',
            '/*from*',
            '/*gclid*',
            '/*yclid*',
        ]);

        $disallows_string = $this->implodeColumn(
            $disallows,
            function ($disallow) {
                return 'Disallow: ' . $disallow;
            },
            "\n"
        );

        $content = $this->implodeColumn(
            ['Yandex', 'Googlebot', '*'],
            function ($robot) use ($disallows_string) {
                return 'User-Agent: ' . $robot . "\n" . $disallows_string;
            },
            "\n\n"
        );

        $content .= "\n\n";

        $content .= 'Host: ' . $this->domain . "\n";
        $content .= 'Sitemap: ' . $this->domain . "/sitemap.xml\n\n";

        file_put_contents($this->output_path, $content);
    }

    public function implodeColumn(array $array, $attribute, $delimiter = '')
    {
        $result = '';
        $length = count($array);
        $is_first = true;
        for ($i = 0; $i < $length; ++$i) {
            if ($attribute instanceof \Closure) {
                $value = $attribute->__invoke($array[$i], $i);
            } else {
                $value = $array[$i]->{$attribute};
            }

            if (empty(trim($value))) {
                continue;
            }

            if (!$is_first) {
                $result .= $delimiter;
            }
            $result .= $value;

            $is_first = false;
        }

        return $result;
    }
}
