<?php

namespace common\components\sitemap;

use yii\base\Object;

/**
 * Class SiteMapsLinker
 * @author German Sokolov
 * @package common\components\sitemap
 */
class SiteMapsLinker extends Object
{
    /**
     * @var SiteMapConfig[]
     */
    public $configs;

    /**
     * @var string
     */
    private $first_map_path = null;

    /**
     * @var string|null
     */
    private $first_domain;

    /**
     *
     */
    public function generateAll()
    {
        foreach ($this->configs as $config) {
            foreach (array_keys($config->provider->getList()) as $domain) {
                $this->generateMapForWithRepeat($config, $domain);
                $this->generateRobotsFor($config, $domain);
            }
            //Генерация для основного домена без поддоменов
            $this->generateMapForWithRepeat($config, null);
            $this->generateRobotsFor($config, null);

            //Очистить указатель на первую сгенерированную карту, чтобы карты не копировались между сайтами
            $this->first_map_path = null;
        }
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     */
    private function generateRobotsFor($config, $domain)
    {
        $robots = new RobotsGenerator([
            'disallow' => $config->disallow,
            'domain' => $this->getUrl($config, $domain),
            'output_path' => $this->getPath($config, $domain, 'robots.txt'),
        ]);
        $robots->generate();
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     */
    private function generateMapFor($config, $domain)
    {
        $map = new SiteMapGenerator([
            'domain' => $this->getUrl($config, $domain),
            'last_mod' => $config->last_mod,
            'output_path' => $this->getPath($config, $domain, 'sitemap.xml'),
            'disallow' => $config->disallow,
        ]);
        $map->generate();
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     */
    private function generateMapForWithRepeat($config, $domain)
    {
        if ($config->for_each) {
            $this->generateMapFor($config, $domain);
        } else {
            if (!isset($this->first_map_path)) {
                $this->generateMapFor($config, $domain);
                $this->first_map_path = $this->getPath($config, $domain, 'sitemap.xml');
                $this->first_domain = $domain;
            } else {
                $this->copyAs($config, $domain);
            }
        }
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     */
    private function copyAs($config, $domain)
    {
        $first_one = file_get_contents($this->first_map_path);
        $was = '<loc>'. $this->getUrl($config, $this->first_domain);
        $will_be = '<loc>'. $this->getUrl($config, $domain);
        $new_one = str_replace($was, $will_be, $first_one);
        file_put_contents($this->getPath($config, $domain, 'sitemap.xml'), $new_one);
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     * @return string
     */
    private function getUrl($config, $domain)
    {
        return $config->protocol . '://' . ($domain ? $domain . '.' . $config->main_domain : $config->main_domain);
    }

    /**
     * @param SiteMapConfig $config
     * @param string|null $domain
     * @param string $name
     * @return string
     */
    private function getPath($config, $domain, $name)
    {
        $dir = rtrim($config->sites_maps_path, '/') . '/';
        return $domain
            ? $dir . $domain . '.' . $name
            : $dir . $name;
    }
}
