<?php

namespace common\components\sitemap;

use common\components\subdomains\SubdomainsProviderInterface;
use yii\base\Object;

/**
 * Class SiteMapConfig
 * @author German Sokolov
 * @package common\components\sitemap
 */
class SiteMapConfig extends Object
{
    /**
     * @var string
     */
    public $protocol = 'http';

    /**
     * @var SubdomainsProviderInterface
     */
    public $provider;

    /**
     * @var string
     */
    public $main_domain;

    /**
     * @var string
     */
    public $sites_maps_path;

    /**
     * @var string[]
     */
    public $disallow = [];

    /**
     * @var string
     */
    public $last_mod;

    /**
     * @var bool
     */
    public $for_each = false;
}
