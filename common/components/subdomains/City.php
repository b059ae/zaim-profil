<?php

namespace frontends\ds2\components\subdomains;

use yii\base\Object;
use yii\helpers\Url;

/**
 * Class City
 * @author German Sokolov
 * @package frontends\ds2\components\subdomains
 */
class City extends Object
{
    /**
     * @var string
     */
    public $subdomain;

    /**
     * @var Region
     */
    public $region;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $prepositional_case;

    /**
     * @return string
     */
    public function phraseIn()
    {
        return ' в ' . $this->prepositional_case;
    }
}
