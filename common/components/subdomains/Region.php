<?php

namespace frontends\ds2\components\subdomains;

use yii\base\Object;

/**
 * Class Region
 * @author German Sokolov
 * @package frontends\ds2\components\subdomains
 */
class Region extends Object
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $prepositional_case;
}
