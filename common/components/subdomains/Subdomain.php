<?php

namespace common\components\subdomains;

use yii\base\Object;

/**
 * Class Subdomain
 * @author German Sokolov
 * @package common\components\subdomains
 */
class Subdomain extends Object
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $prepositional_case;

    /**
     * @var string
     */
    public $seo_title;

    /**
     * @var string
     */
    public $seo_desc;

    /**
     * @var string
     */
    public $url;

    /**
     * @var bool
     */
    public $is_current;

    /**
     * @var array
     */
    public $params = [];
}
