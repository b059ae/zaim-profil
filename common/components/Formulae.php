<?php

namespace common\components;

use common\models\Option;

/**
 * Class Formulae - это не опечатка, а мн. ч. слова formula
 *
 * @author German Sokolov
 * @package frontends\asdfinans\components
 */
class Formulae
{
    /**
     * Вычисляется процент за месяц по аннуитету на максимальный срок. Чем больше срок, тем меньше % переплата
     *
     * @return string
     */
    public static function getMinOverpayFraction()
    {
        $interest_per_month = (float)Option::get('draggers_interest_per_month_annuity');
        $term_to = (int)Option::get('draggers_term_to');

        return $interest_per_month * (1 + 1 / (pow(1 + $interest_per_month, $term_to) - 1)) - 1 / $term_to;
    }
}
