<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $message;
$this->context->layout = 'error';

?>

<section>

    <div class="container">

        <div class="row">

            <div class="col-sm-12">

                <h1 class="text-center"><?= Html::encode($this->title); ?></h1>

            </div>

        </div>

    </div>

</section>