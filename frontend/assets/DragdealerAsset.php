<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Просто плагин dragdealer
 * Class DragdealerAsset
 * @author German Sokolov
 * @package frontends\asdfinans\assets
 */
class DragdealerAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/views/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/dragdealer.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/dragdealer.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
