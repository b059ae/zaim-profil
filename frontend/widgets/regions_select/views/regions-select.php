<?php

/**
 * @var Subdomain[] $subdomains
 * @var View $this
 */

use yii\web\View;
use common\components\subdomains\Subdomain;

?>

<div class="backcalaasl_z regions-select">
    <select class="js-regions-select regions-select__select">
        <?php foreach ($subdomains as $subdomain) { ?>
            <option value="<?= $subdomain->url ?>" <?= $subdomain->is_current ? 'selected' : '' ?>>
                <?= $subdomain->name ?>
            </option>
        <?php } ?>
    </select>
</div>