<?php

namespace frontend\widgets\regions_select\assets;

use yii\web\AssetBundle;

/**
 * Class RegionsSelectAsset
 * @author German Sokolov
 * @package frontends\asdfinans\widgets\regions_select\widgets
 */
class RegionsSelectAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/widgets/regions_select/views/assets';

    /**
     * @inheritdoc
     */
    public $css = [
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/regions-select.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
