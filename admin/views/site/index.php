<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\SearchApplications */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="applications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('/applications/_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'phone',
            'name',
            'domain',
            //'note',
            //'subject_type',
            // 'city',
            // 'sum',
            // 'description',
            // 'payments_type',
            'created_at:datetime',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'applications',
                'template' => '{view} {delete}',
            ],
        ],
    ]); ?>
