<?php

namespace avto\assets;

use yii\web\AssetBundle;

/**
 * Class AutoNumericAsset
 * @author German Sokolov
 * @package avtos\asdfinans\assets
 */
class AutoNumericAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@avto/views/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/autoNumeric.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
