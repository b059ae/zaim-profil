<?php

namespace avto\assets;

use yii\web\AssetBundle;

/**
 * Готовые бегунки калькулятора ПСК
 * Class LoanDraggersAsset
 * @author German Sokolov
 */
class LoanDraggersAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@avto/views/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/number_format.js',
        'js/loan-draggers.js?20180130',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'avto\assets\DragdealerAsset',
        'common\assets\UniformAsset',
        'common\assets\UnderscoreAsset',
        'avto\assets\AutoNumericAsset',
    ];
}
