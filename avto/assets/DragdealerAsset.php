<?php

namespace avto\assets;

use yii\web\AssetBundle;

/**
 * Просто плагин dragdealer
 * Class DragdealerAsset
 * @author German Sokolov
 * @package avtos\asdfinans\assets
 */
class DragdealerAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@avto/views/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/dragdealer.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/dragdealer.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
