<?php

namespace avto\widgets\regions_select;

use common\components\subdomains\Subdomains;
use avto\widgets\regions_select\assets\RegionsSelectAsset;
use yii\base\Widget;
use yii\helpers\Json;
use yii\web\View;

/**
 * Class RegionsSelect
 * @author German Sokolov
 * @package avtos\asdfinans\widgets\regions_select
 */
class RegionsSelect extends Widget
{
    /**
     * @var string
     */
    public $view_path = '@avto/widgets/regions_select/views/regions-select';

    /**
     * @var Subdomains
     */
    public $subdomains;

    /**
     * @return void
     */
    public function init()
    {
        $this->registerJs($this->view);
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render($this->view_path, [
            'subdomains' => $this->subdomains->getSubdomains(),
            'current_subdomain' => $this->subdomains->getCurrent(),
        ]);
    }

    /**
     * @param View $view
     */
    public function registerJs(View $view)
    {
        RegionsSelectAsset::register($view);


        $view->registerJs("
            new RegionsSelect();
        ");
    }
}
