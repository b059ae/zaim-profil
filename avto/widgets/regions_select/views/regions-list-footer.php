<?php

/**
 * @var Subdomain[] $subdomains
 * @var Subdomain $current_subdomain
 * @var View $this
 */

use yii\web\View;
use common\components\subdomains\Subdomain;

?>

<div class="regions-list-footer block">
    <h2 class="regions-list-footer__title footer-title">Регионы присутствия</h2>
    <div class="regions-list-footer__list-block">
        <ul class="regions-list-footer__list">
            <?php foreach ($subdomains as $subdomain) { ?>
                <li class="regions-list-footer__item <?= $subdomain->is_current ? 'regions-list-footer__item--selected' : '' ?>">
                    <?php if ($subdomain->is_current) { ?>
                        <?= $subdomain->name ?>
                    <?php } else { ?>
                        <a class="regions-list-footer__link" href="<?= $subdomain->url ?>">
                            <?= $subdomain->name ?>
                        </a>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>