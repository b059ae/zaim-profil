function ajaxErrorHandle(xhr) {
    if (xhr.status.toString().charAt(0) == 5) {
        alert('Ошибка ' + xhr.status + '. Неполадки на сервере. Пожалуйста, попробуйте позднее или обратитесь по телефону.');
    } else if (xhr.status.toString().charAt(0) == 4) {
        alert('Ошибка ' + xhr.status + '. Неизвестная проблема отправки.');
    } else {
        alert(xhr.status + '. Неизвестная ошибка.');
    }
}

$(function () {

    var send_call_order_url = $('input[name="send_call_order_url"]').val();
    var apply_url = $('input[name="apply_url"]').val();

    var $call_order_buttons = $(".buttonsendfcall, .footer-send-button");
    $call_order_buttons.click(function () {// перехватываем все при событии отправки
        $('body').css("cursor", "progress");

        var form = $(this).closest('form'); // запишем форму, чтобы потом не было проблем с this
        var error = false; // предварительно ошибок нет
        form.find('input, textarea').not('input[name="CallOrder[time]"]').each(function () {
            var that = $(this); // пробежим по каждому полю в форме
            if (that.val() == '') { // если находим пустое
                that.addClass('input-error'); // говорим заполняй!
                error = true; // ошибка
            } else {
                that.removeClass('input-error');
            }
        });
        if (!error) { // если ошибки нет
            var data = form.serialize(); // подготавливаем данные
            $.ajax({ // инициализируем ajax запрос
                type: 'POST', // отправляем в POST формате, можно GET
                url: send_call_order_url, // путь до обработчика, у нас он лежит в той же папке
                dataType: 'json', // ответ ждем в json формате
                data: data, // данные для отправки
                beforeSend: function (data) { // событие до отправки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
                },
                success: function (data) { // событие после удачного обращения к серверу и получения ответа
                    if (data['error']) { // если обработчик вернул ошибку
                        alert(data['error']['time']);
                    } else {
                        $('.call').css('height', 'initial');
                        $('.callform').css('display', 'none');
                        $('.calltitle').text('Заявка успешно отправлена!');
                        $('.overpage').css('display', 'block');
                        setTimeout(function () {
                            $('.overpage').css('opacity', '1');
                        }, 100);
                        form.find('input[type="text"], textarea').val('');
                    }

                },
                error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
                    ajaxErrorHandle(xhr);
                },
                complete: function (data) { // событие после любого исхода
                    form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
                    $('body').css("cursor", "default");
                }
            });
        }
        return false; // вырубаем стандартную отправку формы
    });

    $call_order_buttons.closest('form').find('input, textarea').blur(function () {
        var that = $(this);
        if (that.val() != '') {
            that.removeClass('input-error');
        }
    });

    $(".buttonsendfcall_z").click(function () {// перехватываем все при событии отправки
        $('body').css("cursor", "progress");
        var form = $('.backcalaasl_z'); // запишем форму, чтобы потом не было проблем с this
        var error = false; // предварительно ошибок нет
        form.find('input, textarea').each(function () {
            var that = $(this); // пробежим по каждому полю в форме
            if (that.val() == '') { // если находим пустое
                that.addClass('input-error'); // говорим заполняй!
                error = true; // ошибка
            }
        });
        if (!error) { // если ошибки нет
            var data = form.serialize(); // подготавливаем данные
            $.ajax({ // инициализируем ajax запрос
                type: 'POST', // отправляем в POST формате, можно GET
                url: apply_url, // путь до обработчика, у нас он лежит в той же папке
                dataType: 'json', // ответ ждем в json формате
                data: data, // данные для отправки
                beforeSend: function (data) { // событие до отправки
                    form.find('input[type="submit"]').attr('disabled', 'disabled'); // например, отключим кнопку, чтобы не жали по 100 раз
                },
                success: function (data) { // событие после удачного обращения к серверу и получения ответа
                    if (data['error']) {
                        alert(data['error']);
                    } else { // если все прошло ок
                        $('.call_z').css('height', 'initial');
                        $('.callform_z').css('display', 'none');
                        $('.calltitle_z').text('Заявка успешно отправлена!');
                        $('.overpage_z').css('display', 'block');
                        setTimeout(function () {
                            $('.overpage_z').css('opacity', '1');
                        }, 100)
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случае неудачного завершения запроса к серверу
                    ajaxErrorHandle(xhr);
                },
                complete: function (data) { // событие после любого исхода
                    form.find('input[type="submit"]').prop('disabled', false); // в любом случае включим кнопку обратно
                    $('body').css("cursor", "default");
                }

            });
        }
        return false; // вырубаем стандартную отправку формы
    });
});	