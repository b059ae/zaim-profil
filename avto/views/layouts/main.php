<?php

use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $content string */

//use backend\modules\content\components\widgets\DownloadablesList;
use common\models\Option;

$subdomain = Yii::$app->subdomains->getCurrent();

$options = [
    'center' => $subdomain->params['coords'],
    'zoom' => $subdomain->params['map_zoom'],
    'placemarks' => [
        [
            'coords' => $subdomain->params['coords'],
            'number' => 0,
            'title' => $subdomain->params['address'],
        ],
    ],
];

?>

<?php $this->beginContent('@app/views/layouts/base.php'); ?>

    <section id="calculator">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 blog-main">
                    <div class="slide"></div>
                    <?= $this->render('@app/views/site/_draggers'); ?>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="info phrases-block">
            <div class="container">
                <div class="slide" id="slide-2">&nbsp;</div>
                <div class="col4" data-animated="bounceIn">
                    <div class="phrases-block_icon-wrapper">
                        <img src="/images/summa-zaima-icon.png" alt="" class="phrases-block_icon">
                    </div>
                    <div class="num phrases-block_profit-txt">Сумма займа
                        от </br><?= number_format(Option::get('draggers_sum_from'), 0, ',', ' ') ?>
                        до <?= number_format(Option::get('draggers_sum_to'), 0, ',', ' ') ?>&nbsp; <span
                                class="rub">a</span></div>
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="phrases-block_icon-wrapper">
                        <img src="/images/calendar-and-clock-icon.png" alt="" class="phrases-block_icon">
                    </div>
                    <div class="num">До 36 месяцев с досрочным</br>погашением без переплат</div>
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="phrases-block_icon-wrapper">
                        <img src="/images/auto-stay-with-you-icon.png" alt="" class="phrases-block_icon">
                    </div>
                    <div class="num">Автомобиль</br>остается у Вас</div>
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="phrases-block_icon-wrapper">
                        <img src="/images/do-70-percents-icon.png" alt="" class="phrases-block_icon">
                    </div>
                    <div class="num">До 70% от оценочной</br>стоимости авто</div>
                </div>
            </div>
        </div>
    </section>

<?= $content; ?>

    <section id="faq">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slide" id="slide-5"></div>
                    <div class="h2"><h2>Часто задаваемые вопросы</h2></div>
                    <div class="ac-container">

                        <div class="row">
                            <div class="col-md-6 col-xs-12 faq_questions">
                                <ul class="faq_questions-items">
                                    <li class="faq_questions-item active clearfix"><span>Какие требуются документы для оформления займа?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix"><span>Как получить деньги по займу?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix">
                                        <span>Вносится ли запись в ПТС о залоге?</span></li>
                                    <li class="faq_questions-item clearfix"><span>Требуется ли переоформление транспортного средства для получения займа?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix"><span>Можно ли оформить займ по генеральной доверенности на транспортное средств?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix"><span>Требования к заемщику</span></li>
                                    <li class="faq_questions-item clearfix"><span>Требования к залогу (авто)</span></li>
                                    <li class="faq_questions-item clearfix">
                                        <span>Возможно ли досрочное погашение займа?</span></li>
                                    <li class="faq_questions-item clearfix"><span>Как быстро вернуть ПТС после погашения займа?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix"><span>Влияет ли кредитная история на решение о выдаче займа?</span>
                                    </li>
                                    <li class="faq_questions-item clearfix"><span>Какие дополнительные расходы несет заёмщик?</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-xs-12 faq_answers" id="faq_answers">
                                <div class="faq_answers-item">
                                    Для оформления займа Вам потребуются: паспорт РФ, свидетельство транспортного
                                    средства(СТС), паспорт транспортного средства (ПТС).
                                </div>
                                <div class="faq_answers-item">
                                    Денежные средства будут переведены на Ваш банковский(карточный) счет после
                                    подписания договора займа.
                                </div>
                                <div class="faq_answers-item">
                                    В ПТС пометок не вносится.
                                </div>
                                <div class="faq_answers-item">
                                    Переоформление транспортного средства не требуется, займ выдается собственнику ТС.
                                </div>
                                <div class="faq_answers-item">
                                    По генеральной доверенности займ оформить нельзя.
                                </div>
                                <div class="faq_answers-item">
                                    Физ. лица 21-65 лет, гражданство РФ.
                                </div>
                                <div class="faq_answers-item">
                                    Авто иностранного производства не старше 12 лет, отечественные не старше 7 лет. ТС
                                    не должно находиться в кредите, залоге или под иными обременениями
                                </div>
                                <div class="faq_answers-item">
                                    Досрочное погашение займа возможно в любой день без комиссий и штрафов.
                                </div>
                                <div class="faq_answers-item">
                                    Птс будет возвращен собственнику в течение суток после погашения займа.
                                </div>
                                <div class="faq_answers-item">
                                    Отрицательная кредитная история не повод для отказа в предоставлении займа. Каждая
                                    заявка рассматривается индивидуально.
                                </div>
                                <div class="faq_answers-item">
                                    Никаких скрытых комиссий нет. Заявленные условия полностью соответствуют реальным.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="whom">
        <div class="container">
            <div class="row">
                <div class="h2">
                    <h2>Кто может получить заём?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3 who-can-items">
                    <div class="who-can-item clearfix">
                        <div class="who-can-item-icon">
                            <img src="/images/fizlica.png" alt="">
                        </div>
                        <span class="who-can-item-descr">
                            <span class="who-can-item-descr_head">Физические лица</span><span
                                    class="who-can-item-descr_subhead">Возраст от 21 года и соответствующие документы, подтверждающие владение автомоблем.</span>
                        </span>
                    </div>
                </div>
                <?php /*
                <div class="col-md-6 who-can-items">
                    <div class="who-can-item clearfix">
                        <div class="who-can-item-icon">
                            <img src="/images/yurlica.png" alt="">
                        </div>
                        <span class="who-can-item-descr">
                        <span class="who-can-item-descr_head">Юридические лица</span><span class="who-can-item-descr_subhead">Являющиеся коммерческими организациями, резидентами РФ, созданными не менее 1 (Одного) года до подачи заявки о предоставлении займа.</span>
                        </span>
                    </div>
                </div>
                */ ?>
            </div>
        </div>
    </section>

    <section class="steps">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="step-item">
                        <span class="step-item-num">1 шаг</span>
                        <div class="step-item-icon-wrapper">
                            <img src="/images/steps-icon1.png" alt="" class="step-item-icon">
                        </div>
                        <span class="step-txt">Оставить заявку на сайте или позвонить на горячую линию</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="step-item">
                        <span class="step-item-num">2 шаг</span>
                        <div class="step-item-icon-wrapper">
                            <img src="/images/steps-icon2.png" alt="" class="step-item-icon">
                        </div>
                        <span class="step-txt">Приехать в офис с пакетом документов: </span>
                        <ul class="step-ul">
                            <li>Паспорт РФ,</li>
                            <li>ПТС - Паспорт транспортного средства,</li>
                            <li>СТС - Свидетельство о регистрации транспортного средства</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="step-item">
                        <span class="step-item-num">3 шаг</span>
                        <div class="step-item-icon-wrapper">
                            <img src="/images/steps-icon3.png" alt="" class="step-item-icon">
                        </div>
                        <span class="step-txt">Заключить договор и получить деньги (машина остается у вас)</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="slide" id="slide-6"></div>
                    <div class="h2"><h2>О компании</h2></div>
                    <div class="about-block">
                        <?php /*<p>Общество с ограниченной ответственностью <b>&laquo;МИКРОКРЕДИТНАЯ КОМПАНИЯ ИНВЕСТПРОФИЛЬ&raquo;</b> является лидером Юга России в сфере выдачи займов под залог недвижимости.</p> */ ?>
                        <p>Общество с ограниченной ответственностью <b>&laquo;МИКРОКРЕДИТНАЯ КОМПАНИЯ ИНВЕСТПРОФИЛЬ&raquo;</b>
                            зарегистрировано в соответствии с Федеральным законом 02.07.2010 N 151-ФЗ <b>&laquo;О
                                микрофинансовой деятельности и микрофинансовых организациях&raquo;</b> в государственном
                            реестре микрофинансовых организаций 15 ноября 2017 года за номером 1704060008593
                            (Свидетельство о внесении сведений о юридическом лице в государственный реестр
                            микрофинансовых организаций № 004733).</p>
                        <p>Надзорным и регулирующим органом является Центральный банк Российской Федерации.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 docs clearfix">
                    <div class="col-md-6 col-sm-12">
                        <div class="docs-item">
                            <span class="head clearfix">
                                <span class="head_1">Документы</span><span class="head_2">загрузить</span>
                            </span>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">Базовый стандарт СРО для МФО (утвержден 22.06.2017)</div>
                                <a href="/downloads/Базовый_стандарт_СРО_для_МФО_(утвержден_22.06.2017).pdf" target="_blank"
                                   class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">Дополнительная информация о займах, привлекаемых Обществом</div>
                                <a href="/downloads/Дополнительная_информация_о_займах,_привлекаемых_Обществом.pdf"
                                   target="_blank" class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Информация об условиях предоставления, пользования и возврата микрозайма <strong>АВТО</strong>
                                </div>
                                <a href="/downloads/Информация_об_условиях_предоставления,_пользования_и_возврата_микрозайма_АВТО.pdf" target="_blank"
                                   class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Общие условия договора микрозайма <strong>АВТО</strong>
                                </div>
                                <a href="/downloads/Общие_условия_договора_микрозайма_АВТО.pdf" target="_blank"
                                   class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Свидетельство МФО ИНВЕСТПРОФИЛЬ
                                </div>
                                <a href="/downloads/Свидетельство_МФО_ИНВЕСТПРОФИЛЬ.pdf" target="_blank"
                                   class="docs_download-lnk">&nbsp;</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="docs-item">
                            <span class="head clearfix">
                                <span class="head_1">Документы</span><span class="head_2">загрузить</span>
                            </span>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Памятка по оформлению обращений в Общество
                                </div>
                                <a href="/downloads/Памятка_по_оформлению_обращений_в_Общество.pdf"
                                   target="_blank" class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Правила предоставления микрозайма <strong>АВТО</strong>
                                </div>
                                <a href="/downloads/Правила_предоставления_микрозайма_АВТО.pdf"
                                   target="_blank" class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Схема взаимодействия Общества и лиц, оказывающих существенное влияние на решения
                                </div>
                                <a href="/downloads/Схема_взаимодействия_Общества_и_лиц,_оказывающих_существенное_влияние_на_решения.pdf"
                                   target="_blank" class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                                <div class="docs-name">
                                    Список аффилированных лиц Общества
                                </div>
                                <a href="/downloads/Список_аффилированных_лиц_Общества.pdf"
                                target="_blank" class="docs_download-lnk">&nbsp;</a>
                            </div>
                            <div class="docs-item_row clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contacts" id="contacts">
        <div class="container">
            <div class="row">
                <div class="h2">
                    <h2>Контакты</h2>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <input type="hidden" name="map-data" value='<?= Json::encode($options) ?>'/>
                </div>
            </div>
        </div>
    </section>

<?php $this->endContent(); ?>