<?php

use common\models\CallOrderForm;
use common\models\Option;
use avto\assets\AppAsset;
use avto\models\ApplicationForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$subdomain = Yii::$app->subdomains->getCurrent();

$this->title = $this->title ?? $this->params['title'] ?? $subdomain->seo_title;
$this->params['seo_description'] = $this->params['seo_description'] ?? $subdomain->seo_desc;
$this->params['seo_keywords'] = $this->params['seo_keywords'] ?? 'Займы под залог птс, деньги под залог птс, заложить птс в Ростове-на-Дону, ссуда под залог птс, кредит под залог птс';

$is_subdomain = count(explode('.', $_SERVER['HTTP_HOST'])) > 2;

$header = $this->params['header'] ?? 'Займы под залог птс';
$is_home = $this->params['is_home'] ?? false;
?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title); ?></title>

        <?php if (!empty($this->params['seo_description'])) : ?>
            <meta name="description" content="<?= $this->params['seo_description']; ?>">
        <?php endif; ?>

        <?php if (!empty($this->params['seo_keywords'])) : ?>
            <meta name="keywords" content="<?= $this->params['seo_keywords']; ?>">
        <?php endif; ?>

        <meta name="format-detection" content="telephone=no">
        <meta name='yandex-verification' content='5619d78c10d44998'/>
        <meta name="google-site-verification" content="vXK-KuuEVNHXanS2SMUIgBBHOY1absnk0P0qhh6GhbI"/>

        <meta property="og:title" content="Займы под залог птс"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://zaim-profil.ru"/>
        <meta property="og:image" content="https://zaim-profil.ru/images/headerbg.jpg"/>
        <meta property="og:locale" content="ru-RU"/>
        <meta property="og:description"
              content="Компания «АСД-ФИНАНС» предоставляет денежные займы под залог птс на Юге России. Сумма займа начинается от 100 тысяч и достигает 500 тысяч рублей (под низкие проценты)."/>

        <?= Html::csrfMetaTags(); ?>

        <?php $this->head(); ?>

        <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon"/>

        <!--[if lt IE 9]>
        <script src="../../assets/js/ie8-responsive-file-warning.js"></script>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-78987997-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-78987997-2');
        </script>
    </head>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter47125872 = new Ya.Metrika({
                        id:47125872,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/47125872" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <body>

    <input type="hidden" name="send_call_order_url" value="<?= Url::to(['/site/order-call']); ?>"/>
    <input type="hidden" name="apply_url" value="<?= Url::to(['/site/apply']); ?>"/>

    <div class="overpage">
        <div class="call">
            <div class="close">X</div>
            <div class="calltitle">Заказ обратного звонка</div>
            <div class="callnote">
                <p>
                    Звонок по России бесплатный
                </p>
                <p>
                    Работа call-центра
                    с <?=Option::get('call_order_schedule_call_begin')?>
                    до <?=Option::get('call_order_schedule_call_end')?>
                </p>
            </div>
            <div class="callform">
                <?php
                $call_order = new CallOrderForm();

                $call_order_form = ActiveForm::begin([
                    'action' => Url::to(['/site/order-call']),
                    'options' => ['class' => 'backcalaasl'],
                    'enableClientValidation' => false,
                ]);

                echo $call_order_form
                    ->field($call_order, 'name')
                    ->label(false)
                    ->textInput([
                        'class' => 'formname formfield minformfield',
                        'placeholder' => 'Ваше имя',
                    ]);

                echo $call_order_form
                    ->field($call_order, 'phone')
                    ->label(false)
                    ->widget(MaskedInput::className(), [
                        'class' => 'formfield minformfield',
                        'mask' => '+7 (999) 999-99-99',
                        'options' => [
                            'placeholder' => 'Ваш телефон',
                        ],
                        'clientOptions' => [
                            'showMaskOnHover' => false,
                        ],
                    ]);
                ?>
                <div class="text-center">
                    <input type="button" name="send" class="buttonsendfcall" value="ОТПРАВИТЬ"
                           onclick="yaCounter47125872.reachGoal('zakazat-zvonok-send');">
                </div>
                <?php $call_order_form->end(); ?>
            </div>
        </div>
    </div>

    <div class="overpage_z">
        <div class="call_z">
            <div class="close_z">X</div>
            <div class="calltitle_z">Предварительная заявка на займ</div>
            <div class="callform_z">
                <?php
                $application = new ApplicationForm();
                $application_form = ActiveForm::begin([
                    'action' => Url::to(['/site/apply']),
                    'options' => ['class' => 'backcalaasl_z'],
                    'enableClientValidation' => false,
                ]);
                ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'name') ?>"
                        placeholder="<?= $application->getAttributeLabel('name') ?>"
                        title="<?= $application->getAttributeLabel('name') ?>"
                        size="1"
                        class="formname formfield minformfield width_big"
                />
                <?= Html::activeDropDownList($application, 'subject_type', ApplicationForm::getSubjectTypesList(), [
                    'class' => 'formfield minformfield width_big',
                    'size' => '1',
                    'title' => $application->getAttributeLabel('subject_type'),
                ]) ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'city') ?>"
                        placeholder="<?= $application->getAttributeLabel('city') ?>"
                        title="<?= $application->getAttributeLabel('city') ?>"
                        size="1"
                        class="formfield minformfield width_big"
                />
                <?= MaskedInput::widget([
                    'model' => $application,
                    'attribute' => 'phone',
                    'mask' => '+7 (999) 999-99-99',
                    'options' => [
                        'class' => 'formfield minformfield width_big',
                        'size' => '1',
                        'placeholder' => $application->getAttributeLabel('phone'),
                        'title' => $application->getAttributeLabel('phone'),
                    ],
                ]) ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'sum') ?>"
                        placeholder="<?= $application->getAttributeLabel('sum') ?>"
                        title="<?= $application->getAttributeLabel('sum') ?>"
                        size="1"
                        class="formfield minformfield width_big"
                />
                <textarea
                        name="<?= Html::getInputName($application, 'description') ?>"
                        placeholder="<?= $application->getAttributeLabel('description') ?>"
                        title="<?= $application->getAttributeLabel('description') ?>"
                        class="formfield minformfield width_big"
                ></textarea>
                <input type="button" name="send" class="buttonsendfcall_z" onclick="yaCounter47125872.reachGoal
                ('zaivka-send');" value="ОТПРАВИТЬ">
                <?php $application_form->end(); ?>
            </div>
        </div>
    </div>
    <!-- HEADER -->
    <div class="header">

        <?php /*
        echo RegionsSelect::widget([
            'subdomains' => \Yii::$app->subdomains,
            'view_path' => '@avto/widgets/regions_select/views/regions-list-mobile',
        ]); */
        ?>

        <div class="collapse navbar-collapse">
            <div class="container">
                <a href="/" class="col-sm-3 col-xs-5 logo">
                    <img src="/images/logo.png?20180120" alt="">
                </a>
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#calculator" onclick="return anchorScroller(this)">
                            Калькулятор
                        </a>
                    </li>
                    <li>
                        <a href="#faq" onclick="return anchorScroller(this)">
                            Часто задаваемые вопросы
                        </a>
                    </li>
                    <li>
                        <a href="#whom" onclick="return anchorScroller(this)">
                            Кто может получить заём
                        </a>
                    </li>
                    <li>
                        <a href="#about" onclick="return anchorScroller(this)">
                            О компании
                        </a>
                    </li>
                    <li>
                        <a href="#contacts" onclick="return anchorScroller(this)">
                            Контакты
                        </a>
                    </li>
                </ul>
                <a href="tel:88005504109" class="navbar-collapse_phonecall">8 (800) 550 41 09</a>
            </div>
        </div>

        <div class="container">
            <div class="col-sm-12 col-md-8">
                <h1 class="title">
                    <span class="title_head_g title_head_b">Займы</span><br/>
                    <span class="title_head_subtitle">под залог <span class="title_head_g">птс</span></span>
                    <div class="title_head_city">в Ростове-на-Дону</div>
                </h1>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="topcont pull-right lead-form-top">
                    <a href="tel:88005504109" class="lead-form-top-row clearfix">
                        <img class="lead-form-top-icon" src="/images/phone-icon.png"></img>
                        <span class="lead-form-top-descr lead-form-top-descr_phone">8 (800) 550 41 09</span>
                    </a>
                    <a href="mailto:info@zaim-profil.ru" class="lead-form-top-row clearfix">
                        <img class="lead-form-top-icon" src="/images/email-icon.png"></img>
                        <span class="lead-form-top-descr lead-form-top-descr_email">info@zaim-profil.ru</span>
                    </a>
                    <div class="lead-form-top-row clearfix">
                        <img class="lead-form-top-icon" src="/images/clock-icon.png"></img>
                        <span class="lead-form-top-descr lead-form-top-descr_dop">Звонок по России бесплатный<br/>Работа call-центра с <?=Option::get('call_order_schedule_call_begin')?> до <?=Option::get('call_order_schedule_call_end')?></span>
                    </div>
                    <a
                            href="#"
                            class="backcall lead-form-top_lead-btn"
                            onclick="yaCounter47125872.reachGoal('zakazat-zvonok-click'); ga('send', 'event', 'zakazat-zvokok', 'click');"
                    >Заказать звонок</a>
                    <span class="lead-form-top_lead-descr"><?= Option::get('address_short') ?></span>
                </div>
            </div>
        </div>
        <div class="container profits clearfix">
            <div class="profits-item">
                <div class="profits-item_icon-wrapper">
                    <img src="/images/driver-license-icon.png" alt="" class="profits-item_icon">
                </div>
                <span class="profits-item_descr">Автомобиль остается у владельца</span>
            </div>
            <div class="profits-item">
                <div class="profits-item_icon-wrapper">
                    <img src="/images/pie-chart-icon.png" alt="" class="profits-item_icon">
                </div>
                <span class="profits-item_descr">До 70% от оценочной стоимости авто</span>
            </div>
            <div class="profits-item">
                <div class="profits-item_icon-wrapper">
                    <img src="/images/money-value-icon.png" alt="" class="profits-item_icon">
                </div>
                <span class="profits-item_descr">
                    От <?= number_format(Option::get('draggers_sum_from'), 0, ',', ' ') ?>
                    до <?= number_format(Option::get('draggers_sum_to'), 0, ',', ' ') ?>
                    рублей
                </span>
            </div>
            <div class="profits-item">
                <div class="profits-item_icon-wrapper">
                    <img src="/images/business-agreement-icon.png" alt="" class="profits-item_icon">
                </div>
                <span class="profits-item_descr">Досрочное погашение без комиссии и переплат</span>
            </div>
            <div class="profits-item">
                <div class="profits-item_icon-wrapper">
                    <img src="/images/bar-graph-with-dollar-sign-icon.png" alt="" class="profits-item_icon">
                </div>
                <span class="profits-item_descr">Получение  денег за 1 день</span>
            </div>
        </div>
    </div>

    <div class="navbar-header">
        <div class="logo"><img src="/images/logo_white.png" alt="" title=""/></div>
        <div class="phone">
            <?php
            $phone = Option::get('phones_header_phone');
            ?>
            <a href="tel:<?= preg_replace('[^0-9\+]', '', $phone) ?>" class="cpc-tel">
                <?= $phone ?>
            </a>
        </div>
    </div>
    <!-- /HEADER -->


    <?php
    if (extension_loaded('newrelic')) {
        echo newrelic_get_browser_timing_header();
    }
    $this->beginBody();
    ?>

    <main>
        <?= $content ?>

    </main>

    <?php
    $this->endBody();
    if (extension_loaded('newrelic')) {
        echo newrelic_get_browser_timing_footer();
    }
    ?>

    <section id="index-page-map" class="map-holder"></section>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="footer_cont">
                        <div class="footer-phone footer_cont-row clearfix">
                            <div class="footer_cont-row_icon-wrapper">
                                <img src="/images/phone-icon.png" alt="" class="footer-phone_icon">
                            </div>
                            <a href="tel:88005504109" class="footer-phone_val">8 (800) 550 41 09</a>
                        </div>
                        <div class="footer_cont-row clearfix">
                            <div class="footer_cont-row_icon-wrapper">
                                <img src="/images/email-icon.png" alt="" class="footer-phone_icon">
                            </div>
                            <a href="mailto:info@zaim-profil.ru" class="footer-phone_val">info@zaim-profil.ru</a>
                        </div>
                        <div class="footer_cont-row clearfix">
                            <div class="footer_cont-row_icon-wrapper">
                                <img src="/images/clock-icon.png" alt="" class="footer-phone_icon">
                            </div>
                            <span class="footer-phone_val">Звонок по России бесплатный<br/>Работа call-центра с <?=Option::get('call_order_schedule_call_begin')?> до <?=Option::get('call_order_schedule_call_end')?></span>
                        </div>
                        <div class="footer_cont-row text-left clearfix">
                            <span>
                                <?php if ($subdomain->params['address']) : ?>
                                    Наш адрес:<br/>
                                    <?= $subdomain->params['address']; ?>
                                <?php endif; ?>
                            </span>
                        </div>
                    </div>
                    <div class="copira">© <?= date('Y') ?> Общество с ограниченной ответственностью <a href="#">МИКРОКРЕДИТНАЯ КОМПАНИЯ ИНВЕСТПРОФИЛЬ</a></div>
                </div>
                <div class="col-sm-12 col-md-5 second-column pull-right">
                    <div class="footer_form_bg">
                        <?php
                        $call_order = new CallOrderForm();
                        $call_order_form = ActiveForm::begin([
                            'action' => Url::to(['/site/order-call']),
                            'options' => ['class' => ''],
                            'enableClientValidation' => false,
                        ]);
                        ?>
                        <div class="title">У вас остались вопросы?</div>
                        <div class="text">Оставьте свои данные и наш специалист </br>перезвонит вам в ближайшее время</div>

                        <div class="f_input">
                            <?php
                            echo $call_order_form
                                ->field($call_order, 'name')
                                ->label(false)
                                ->textInput([
                                    'class' => 'form_elem',
                                    'placeholder' => 'Ваше имя',
                                ]);
                            ?>
                        </div>
                        <div class="f_input">
                            <?php
                            echo $call_order_form
                                ->field($call_order, 'phone')
                                ->label(false)
                                ->widget(MaskedInput::className(), [
                                    'class' => 'form_elem',
                                    'mask' => '+7 (999) 999-99-99',
                                    'options' => [
                                        'placeholder' => 'Ваш телефон',
                                        'id' => 'callorderform-phone-2',
                                    ],
                                    'clientOptions' => [
                                        'showMaskOnHover' => false,
                                    ],
                                ]);
                            ?>
                        </div>
                        <div class="f_input">
                            <input type="submit" name="mail_sub" value="Отправить" class="footer-send-button">
                        </div>
                        <?php $call_order_form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <?php /*
    <script type="text/javascript"
            src="https://perezvoni.com/files/widgets/4886-cb4c7f8f2cf8345a2364-d5c769cb4c7f8f2-2364-90e5f-c7.js"
            charset="UTF-8"></script>
    */ ?>
    </body>
    </html>
<?php
$this->endPage();
