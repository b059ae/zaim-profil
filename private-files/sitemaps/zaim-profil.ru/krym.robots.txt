User-Agent: Yandex
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

User-Agent: Googlebot
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

User-Agent: *
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

Host: http://krym.zaim-profil.ru
Sitemap: http://krym.zaim-profil.ru/sitemap.xml

