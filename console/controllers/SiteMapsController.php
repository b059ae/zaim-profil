<?php

namespace console\controllers;

use common\components\sitemap\SiteMapConfig;
use common\components\sitemap\SiteMapsLinker;
use yii\console\Controller;

/**
 * Class SiteMapsController
 *
 * @author German Sokolov
 * @package console\controllers
 */
class SiteMapsController extends Controller
{
    /**
     * @param string $date
     * @throws \yii\base\InvalidConfigException
     */
    public function actionMakeAll($date = null)
    {
        if (is_null($date)) {
            $date = date('Y-m-d', time());
        }

        $linker = new SiteMapsLinker([
            'configs' => [
                new SiteMapConfig([
                    'protocol' => 'http',
                    'provider' => \Yii::createObject(
                        include(\Yii::getAlias('@frontend/config/subdomains.php'))
                    ),
                    'last_mod' => $date,
                    'main_domain' => 'zaim-profil.ru',
                    'sites_maps_path' => \Yii::getAlias('@private-files-dir/sitemaps/zaim-profil.ru'),
                    'disallow' => [],
                ])
            ],
        ]);

        $linker->generateAll();

        $linker = new SiteMapsLinker([
            'configs' => [
                new SiteMapConfig([
                    'protocol' => 'http',
                    'provider' => \Yii::createObject(
                        include(\Yii::getAlias('@avto/config/subdomains.php'))
                    ),
                    'last_mod' => $date,
                    'main_domain' => 'zaim-profil.ru',
                    'sites_maps_path' => \Yii::getAlias('@private-files-dir/sitemaps/zaim-profil.ru'),
                    'disallow' => [],
                ])
            ],
        ]);

        $linker->generateAll();
    }
}
