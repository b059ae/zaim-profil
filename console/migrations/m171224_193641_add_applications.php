<?php

use yii\db\Migration;

/**
 * Class m171224_193641_add_applications
 */
class m171224_193641_add_applications extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'phone' => $this->string()->notNull()->comment('Телефон'),
            'name' => $this->string()->notNull()->comment('Имя'),
            'note' => $this->string()->comment('Примечание'),
            'domain' => $this->string()->comment('Домен'),

            'subject_type' => $this->integer()->comment('Тип субъекта'),
            'city' => $this->string()->comment('Город'),
            'sum' => $this->integer()->comment('Сумма'),
            'description' => $this->string()->comment('Описание залога'),
            'payments_type' => $this->integer()->comment('Тип погашения'),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('applications');
    }

}
